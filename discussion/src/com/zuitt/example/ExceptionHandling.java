package com.zuitt.example;

import java.util.Scanner;

public class ExceptionHandling {
    public static void main(String[] args){
        Scanner input = new Scanner(System.in);
        int num = 0;

        System.out.println("Please enter a number: ");
        // try to do/execute the statement
        try {
            num = input.nextInt();
        }
        // catch any errors
        catch (Exception e) {
            System.out.println("Invalid input!");
            e.printStackTrace();
        }
        finally {
            System.out.println("You have entered: " + num);
        }
    }
}
