package com.zuitt.example;

import java.util.Scanner;

public class S3A1 {
    public static void main(String[] args){
        System.out.println("Please enter a number: ");
        Scanner in = new Scanner(System.in);
        int num = 0;
        int numPrime = 0;
        long factorial = 1;
        boolean tooLong = false;

        try {
            numPrime = in.nextInt();
            num = numPrime;
            if (numPrime < 0){
                System.out.println("Invalid input! Number must be positive integers.");
            } else {
                for (int i = num; i >= 1; i--) {
                    factorial=factorial*num;
                    num--;
                    if (factorial <=0) {
                        tooLong = true;
                        break;
                    }
                }
                if (tooLong) {
                    System.out.println("The inputted number is too large to store its factorial.");
                } else {
                    System.out.println("The factorial of " + numPrime + " is : " + factorial);
                }
            }
        } catch (Exception e) {
            System.out.println("Invalid input! Number must be positive integers.");
            e.printStackTrace();
        }
        finally {
            System.out.println("Thank you for running the program.");
        }
    }
}
